## X1Carbon 2013 集成显卡HD4000实现双屏显示
`Yale Wei，2020-10-21，更新于2021-04-21`

主要参考豆瓣网[《Framebuffer数据结构浅析》](https://www.douban.com/note/547235754/)  
链接为：[https://www.douban.com/note/547235754/](https://www.douban.com/note/547235754/)

macOS更新为Catalina（10.15.7），启动引导为OpenCore 0.66，在config.plist中注入的HD4000显卡id（AAPL,ig-platform-id）实现驱动。

后面将介绍三种方法，其中***前两种方法只适用于Catalina及以下版本，最后一种方法通用所有版本***。

在所有各macOS版本（包括Big Sur）中注入的id及结果如下：

| AAPL,ig-platform-id | 内屏 | miniDP外接屏 |
| :-----| :----: | :----: |
| 00006601 | 不亮 | 花屏 |
| 01006601 | 不亮 | 花屏 |
| 03006601 | 屏亮，无显示 | 正常 |
| 04006601 | 正常 | 无法连接 |
| 05006601 | 正常，外接 | 无法连接 |
| 08006601 | 不亮 | 无法连接 |
| 09006601 | 不亮 | 无法连接 |

解决的思路是将03和04整合一起，给Framebuffer打补丁，先将  **/S/L/E**  中的 **AppleIntelFramebufferCapri.kext**拷贝到桌面，打开包，使用十六进制软件**HEX Friend**打开驱动程序**AppleIntelFramebufferCapri**，搜索显卡接口相关信息得出如下几组数据：

```
 第1组  
 00006601 00030403 00000006  
 
 02050000 00040000 07000000  
 03040000 00040000 07000000  
 04060000 00040000 07000000  
```
```
 第2组  
 01006601 01030403 00000006
   
 01000000 02000000 30000000
 02050000 00080000 06000000  
 03040000 00040000 07010000  
 04060000 00040000 07010000  
``` 
```
 第3组  
 03006601 01020402 00000004（platform-id: 01660003, 4个(7th) ports）
 
 05030000 02000000 30000000（索引: 5，总线ID: 0x03，通道: 0，类型: LVDS，标识符: 0x00000030，30意味内建）  
 02050000 00040000 07040000  
 03040000 00040000 81000000  
 04060000 00040000 81000000   
``` 
```
 第4组  
 04006601 01030101 00000002  
 
 05030000 02000000 30020000
```
 有了思路，解决办法跟着就有了。
 
### 方法1：修改FB驱动（不推荐）

对照第4组修改第3组的数据，即

```
 搜索 05 03 00 00 02 00 00 00 30 00 00 00
 替换 05 03 00 00 02 00 00 00 30 02 00 00
```
保存后将**AppleIntelFramebufferCapri.kext**放回**/S/L/E**后，用**hackintool**重建权限及缓存，否则将无法启动。
本方法简单粗暴，但操作非常麻烦，在Catalina还涉及关闭SIP保护等，而且在后续系统更新可能使补丁失效而需重头再来，非常不推荐。

### 方法2：在config.plist打驱动补丁（Big Sur失效）

用ProperTree打开config.plist，在Kernel-Patch下添加com.apple.driver.AppleIntelFramebufferCapri补丁，内容其实就是方法1中的寻找替换部分。

补丁代码如下

```
<?xml version="1.0" encoding="UTF-8"?
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"
<plist version="1.0"
<dict
	<keyKernel</key
	<dict
		<keyPatch</key
		<array
			<dict
				<keyArch</key
				<stringx86_64</string
				<keyBase</key
				<string</string
				<keyComment</key
				<stringFix miniDP out</string
				<keyCount</key
				<integer1</integer
				<keyEnabled</key
				<true/
				<keyFind</key
				<dataBQMAAAIAAAAwAAAA</data
				<keyIdentifier</key
				<stringcom.apple.driver.AppleIntelFramebufferCapri</string
				<keyLimit</key
				<integer0</integer
				<keyMask</key
				<data</data
				<keyMaxKernel</key
				<string</string
				<keyMinKernel</key
				<string</string
				<keyReplace</key
				<dataBQMAAAIAAAAwAgAA</data
				<keyReplaceMask</key
				<data</data
				<keySkip</key
				<integer0</integer
			</dict
		</array
	</dict
</dict
</plist

```
这样就可以无痛升级了。

### 方法3：在config.plist注入设备补丁（推荐）
注入显卡id的同时给连接口打补丁，原理与上述相同。

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>DeviceProperties</key>
	<dict>
		<key>Add</key>
		<dict>
			<key>PciRoot(0x0)/Pci(0x2,0x0)</key>
			<dict>
				<key>AAPL,ig-platform-id</key>
				<data>AwBmAQ==</data>
				<key>framebuffer-con0-busid</key>
				<data>AwAAAA==</data>
				<key>framebuffer-con0-enable</key>
				<data>AQAAAA==</data>
				<key>framebuffer-con0-flags</key>
				<data>MAIAAA==</data>
				<key>framebuffer-con0-index</key>
				<data>BQAAAA==</data>
				<key>framebuffer-con0-pipe</key>
				<data>AAAAAA==</data>
				<key>framebuffer-con0-type</key>
				<data>AgAAAA==</data>
				<key>framebuffer-patch-enable</key>
				<data>AQAAAA==</data>
			</dict>
		</dict>
	</dict>
</dict>
</plist>
```

### 后记：经测试，采用miniDP转HDMI线无法达到4K分辨率，后面买了一条miniDP线，可实现4K30Hz(显卡HD4000支持的最高分辨率)。
---
### 附1：4K补丁
可采用[mac-pixel-clock-patch-V2](https://github.com/floris497/mac-pixel-clock-patch)可实现，具体操作见说明。

最方便的还是用一键HiDPI
[https://github.com/xzhih/one-key-hidpi](https://github.com/xzhih/one-key-hidpi)

### 附2:Intel核显platform ID及smbios表
链接[https://www.it72.com/12550.htm](https://www.it72.com/12550.htm)

详细链接[http://imacos.top/2020/09/03/2216/](http://imacos.top/2020/09/03/2216/)

摘录HD 4000型号如下

| 型号	| platform-id	 | 机型	| 接口	| LVDS	| DP	| HDMI |
| :-----| :----: | :----: | :----: | :----: | :----: | :----: |
| Intel HD Graphics 4000	| 0x01660000 |	| 4 |	1 | 3  |
| Intel HD Graphics 4000	| 0x01660001 |	MacBookPro10,2 | 4 | 1 | 2 | 1
| Intel HD Graphics 4000	| 0x01660002 |	MacBookPro10,1 | 1 | 1 |
| Intel HD Graphics 4000	| 0x01660003 |	MacBookPro9,2 | 4 | 1 | 3 |
| Intel HD Graphics 4000	| 0x01660004 |	MacBookPro9,1 | 1 | 1 |
| Intel HD Graphics 4000	| 0x01620005 |	 | 3 |  	 | 3 |
| Intel HD Graphics 4000	| 0x01620006 |	iMac13,1	 | 0 |
| Intel HD Graphics 4000	| 0x01620007 |	iMac13,2 | 0 |
| Intel HD Graphics 4000	| 0x01660008 |	MacBookAir5,1 | 3 | 1 | 2 |
| Intel HD Graphics 4000	| 0x01660009 |	MacBookAir5,2 | 3 | 1 | 2 |
| Intel HD Graphics 4000	| 0x0166000a |	Macmini6,1 | 3 |  | 2 | 1 |
| Intel HD Graphics 4000	| 0x0166000b |	Macmini6,2 | 3 |  | 2 | 1 |