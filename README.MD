# X1 Carbon 2013黑苹果EFI（OC引导）


`Yale Wei，更新于2021-10-08`
***

此版本参考github上的 [X230](https://github.com/banhbaoxamlan/X230-Hackintosh)的EFI修改。

MacOS版本为Catalina 10.15.7，支持Big Sur，在config.plist文件中修改机型为MacBookAir6,2即可。

SMBIOS机型选用建议：Catalina及以下选MacBookAir5,2或MacBookPro9,2 ，Big Sur选MacBookAir6,2或MacBookPro11,1。

OpenCore为0.6.6版（0.6.7版会无限重启，0.6.8版无法启动），增加一套X1 Carbon的暗黑主题，不喜勿喷。

先启用config-install.plist配置文件（需备USB键鼠）进行系统安装，安装完成并进入系统，然后通过运行tools目录中GenSMBIOS生成三码、ssdtPRGen生成SSDT.aml补丁，相应修改和启用config-run.plist配置文件重启。

**状态: 终结版**

[![ThinkPad](https://img.shields.io/badge/ThinkPad-X1_Carbon-blue.svg)](https://psref.lenovo.com/syspool/Sys/PDF/withdrawnbook/ThinkPad_X1_Carbon_1st_Gen_WE.pdf) [![release](https://img.shields.io/badge/Download-latest-brightgreen.svg)](https://gitee.com/yalewei/x1-carbon2013-hackintosh-efi/releases) [![OpenCore](https://img.shields.io/badge/OpenCore-0.6.6-blue.svg)](https://github.com/acidanthera/OpenCorePkg/releases/latest) [![itlwm](https://img.shields.io/badge/itlwm-2.0-blue.svg)](https://github.com/OpenIntelWireless/itlwm/releases) [![MacOS Catalina](https://img.shields.io/badge/macOS-10.15.7-brightgreen.svg)](https://www.apple.com/macos/catalina/) [![MacOS Big Sur](https://img.shields.io/badge/macOS-11.6-purple.svg)](https://www.apple.com/macos/big-sur/)

***
### Thinkpad X1 Carbon 2013配置

- BIOS Version 2.85
- i7-3667u CPU
- 8GB RAM
- Intel SSD 240GB
- Intel Centrino Advanced-N 6205（开源驱动Itlwm.kext [@zxystd](https://github.com/OpenIntelWireless/itlwm)，由[@pigworlds](https://github.com/OpenIntelWireless/itlwm/commits?author=pigworlds) ported 'iwn' from OpenBSD 后在1.2.0 Alpha中得到支持）

### MacOS驱动情况
 
1. 电源显示正常，合盖睡眠、开盖即刻唤醒，电池续航约2小时；
2. 屏幕亮度调整正常，并显示小太阳；
3. 多媒体快捷键正常，用网易音乐、QQ音乐和itune均正常；
4. 小红点正常，三键正常，触摸手势正常；
5. 声卡（Realtek ALC269，对应layout id请查阅[黑果小兵清单](https://blog.daliansky.net/AppleALC-Supported-codecs.html)）用AppleALC.kext，layout-id为29，插拔耳机切换正常，miniDP输出正常，且无windows下的破音；
6. 用GenSMBIOS生成三码，结合局域网补丁登录FaceTime和Messages正常；
7. 用ssdtPRGen生成SSDT.aml补丁实现变频睿频（需修改ssdtPRGen默认的配置文件ivg bridge.cfg，最低频由800改成900，否则无限重启）；
8. USB2.0和3.0均正常；
9.  WIFI用开源驱动AirportItlwm.kext 2.0稳定版精简编译版正常连接2.4G和5G无线网络，频段40MHz满速300Mbps；
10.  Fn键多媒体功能，下载[ThinkpadAssistant](https://github.com/MSzturc/ThinkpadAssistant/releases)安装后可完整体验；
11. 风扇可用[YogaSMC](https://github.com/zhen-zen/YogaSMC/releases)进行监控；
12. 无痛在线升级。

### 一些未解决问题

1. WIFI无法使用AirDrop，HandOff(catalina单向：手机->电脑，big sur 双向)偶尔能用； 
2. Big Sur比较依赖nvram，当外屏无法输出时，需重置nvram；
3. 读卡器和指纹不要指望。

### 附件工具
新增tools目录，提供黑苹果常用的工具。

| 名称 | git链接 | 主要功能 |
| :-----| :----: | :---- |
| GenSMBIOS| [链接](https://github.com/corpnewt/GenSMBIOS) | 三码生成器，白嫖用 |
| ssdtPRGen| [链接](https://github.com/Piker-Alpha/ssdtPRGen.sh) | 变频补丁生成器 |
| ProperTree| [链接](https://github.com/corpnewt/ProperTree) | plist文件编辑利器 |
| *MacOnlineInstaller*| [链接](https://mianbaoduo.com/o/bread/Y5mTkp8=) | 在线安装MacOS制作工具，仅列举，不提供附件 |
| BT-LinkeySync| [链接](https://github.com/digitalbirdo/BT-LinkkeySync) | 双系统共用蓝牙注册表生成工具 |

## 感谢名单

[Apple](https://www.apple.com) 的macOS

[Acidanthera](https://github.com/acidanthera) 维护的项目

[Rehabman](https://github.com/RehabMan) 和 [黑果小兵](https://github.com/daliansky) 维护的项目

[zxystd](https://github.com/OpenIntelWireless/itlwm) 开发的Intel WIFI驱动

[zhen-zen](https://github.com/zhen-zen/YogaSMC) 开发的YogaSMC驱动和app

[MSzturc](https://github.com/MSzturc) 开发的ThinkpadAssistant

[Dortania](https://dortania.github.io/OpenCore-Install-Guide/)的OpenCore安装指引

[码云](https://gitee.com) 

[github.com](https://github.com) 